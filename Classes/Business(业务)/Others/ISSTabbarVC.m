//
//  ISSTabbarVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSTabbarVC.h"

#import "ISSPersonalVC.h"

@interface ISSTabbarVC ()
{
    NSArray       *titlesArray;
    NSArray       *normalImageArray;
    NSArray       *lightImageArray;
}

@end

@implementation ISSTabbarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    titlesArray = @[@"首页",@"地图",@"消息",@"系统"];
    normalImageArray = @[@"home",@"map",@"message",@"system"];
    lightImageArray = @[@"home-choice",@"map-choice",@"message-choice",@"system-choice"];
    [self createTabbar];
}

- (UIImage *)getImage:(NSString *)imageName
{
    return [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (UITabBarItem *)getTabBarItem:(NSInteger)index
{
    return [[UITabBarItem alloc] initWithTitle:titlesArray[index]
                                         image:[self getImage:normalImageArray[index]]
                                 selectedImage:[self getImage:lightImageArray[index]]];
}

- (void)createTabbar
{
    self.tabBar.tintColor = [UIColor themeColor];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    
    NSMutableArray *navArray = [NSMutableArray new];
    //首页
    
    ISSBaseVC *vc = nil;
    for (int i = 0; i < titlesArray.count; i++)
    {
        if (i == titlesArray.count - 1)
        {
            vc = [[ISSPersonalVC alloc] init];
        }
        else
        {
            vc = [[ISSBaseVC alloc] init];
        }
        vc.navigationItem.title = titlesArray[i];
        ISSNavigationController *nav = [[ISSNavigationController alloc] initWithRootViewController:vc];
        nav.tabBarItem = [self getTabBarItem:i];
        [navArray addObject:nav];
    }
    
    self.viewControllers = navArray.copy;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
