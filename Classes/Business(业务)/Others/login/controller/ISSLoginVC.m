//
//  ISSLoginVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSLoginVC.h"

@interface ISSLoginVC ()

@end

@implementation ISSLoginVC

- (void)loginAction:(UIButton *)btn
{
    [[ISSUserData sharedInstance] saveUserDataWithResponseData:@{@"userId":@"1",@"userPhone":@"13411112222",@"nickname":@"xiongjw"}];
    [ShareApp enterMain:1];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *btn = [ISSCreateUIHelper createSubmitBtnWithTitle:@"login" posY:0];
    btn.center = self.view.center;
    [btn addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
