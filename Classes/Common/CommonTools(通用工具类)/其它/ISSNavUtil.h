//
//  ISSNavUtil.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "X_Button.h"

@interface ISSNavUtil : NSObject

+ (X_Button *)getImageBtn:(NSInteger)btnType;
+ (X_Button *)getImageBtn:(id)delegate
                  btnType:(NSInteger)btnType
              clickAction:(SEL)clickAction;

+ (X_Button *)getTitleBtn:(NSString *)title;
+ (X_Button *)getTitleBtn:(id)delegate
                 btnTitle:(NSString *)btnTitle
              clickAction:(SEL)clickAction;

+ (UIBarButtonItem *)getBarItem:(id)delegate
                        btnType:(NSInteger)btnType
                    clickAction:(SEL)clickAction;

+ (UIBarButtonItem *)getBarItem:(NSInteger)btnType
                     clickBlock:(X_ButtonBlock)block;

+ (UIBarButtonItem *)getBarItemTitle:(NSString*)btnTitle
                          clickBlock:(X_ButtonBlock)block;

@end
