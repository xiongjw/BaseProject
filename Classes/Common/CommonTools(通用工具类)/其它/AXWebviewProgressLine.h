//
//  AXWebviewProgressLine.h
//  AXGY
//
//  Created by xiongjw on 2017/6/22.
//  Copyright © 2017年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXWebviewProgressLine : UIView

//进度条颜色
@property (nonatomic,strong) UIColor  *lineColor;

//开始加载
-(void)startLoadingAnimation;

//结束加载
-(void)endLoadingAnimation;

@end
