//
//  X_Toast.m
//  旅行真人译
//
//  Created by xiongyw on 14-10-31.
//  Copyright (c) 2014年 CC. All rights reserved.
//

#import "X_Toast.h"

#import "X_HUDAlertMsg.h"

#define X_ToastDelay  2.0f

static ToastDismissCompletion _toastDismissCompletion;

@interface X_Toast ()

@end

@implementation X_Toast

+ (void)showErrorMsg:(NSString *)msg toView:(UIView *)view {
    [X_HUDAlertMsg showMsg:msg toView:view];
}

+ (void)dismiss
{
    [SVProgressHUD dismiss];
    if (_toastDismissCompletion) {
        _toastDismissCompletion();
        _toastDismissCompletion = nil;
    }
}

+ (void)showWithStatus:(NSString*)status
{
    [SVProgressHUD showWithStatus:status];
}

+ (void)showInfoWithStatus:(NSString*)status
{
    [SVProgressHUD showInfoWithStatus:status];
}

+ (void)showSuccessWithStatus:(NSString*)status
{
    [self showSuccessWithStatus:status delay:2];
}

+ (void)showSuccessWithStatus:(NSString*)status
                        delay:(NSTimeInterval)delay
{
    [self showSuccessWithStatus:status delay:delay completion:NULL];
}

+ (void)showSuccessWithStatus:(NSString*)status delay:(NSTimeInterval)delay completion:(ToastDismissCompletion)completion
{
    if (completion) {
        _toastDismissCompletion = completion;
    }
    if (delay > 5) {
        [SVProgressHUD setMinimumDismissTimeInterval:delay];
    }
    [SVProgressHUD showSuccessWithStatus:status];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}


/**
 *  error
 *
 *  @param status <#status description#>
 */
+ (void)showErrorWithStatus:(NSString*)status
{
    [self showErrorWithStatus:status delay:2];
}

+ (void)showErrorWithStatus:(NSString*)status delay:(NSTimeInterval)delay
{
    [self showErrorWithStatus:status delay:delay completion:NULL];
}

+ (void)showErrorWithStatus:(NSString*)status delay:(NSTimeInterval)delay completion:(ToastDismissCompletion)completion
{
    
    if (completion) {
        _toastDismissCompletion = completion;
    }
    if (delay > 5) {
        [SVProgressHUD setMinimumDismissTimeInterval:delay];
    }
    [SVProgressHUD showErrorWithStatus:status];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}

/*
examples:
    [X_Toast showErrorWithStatus:@"11111111111"];


    [X_Toast showErrorWithStatus:@"22222222222" delay:1];


    [X_Toast showErrorWithStatus:@"22222222222" delay:.3 completion:^{
        NSLog(@"error.");
    }];

    [X_Toast showErrorWithStatus:@"44444444444" delay:6];


    [X_Toast showSuccessWithStatus:@"55555555555"];

    [X_Toast showSuccessWithStatus:@"66666666666" delay:4];

    [X_Toast showSuccessWithStatus:@"77777777777" delay:.3 completion:^{
        NSLog(@"success.");
    }];

    [X_Toast showSuccessWithStatus:@"88888888888" delay:6];

 */

@end
