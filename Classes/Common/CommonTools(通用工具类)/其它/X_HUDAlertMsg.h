//
//  T_HUDAlertMsg.h
//  PowerMail
//
//  Created by xiongyw on 13-12-16.
//  Copyright (c) 2013年 xiongyw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface X_HUDAlertMsg : NSObject

+ (void)showMsg:(NSString *)msg toView:(UIView *)view;

+ (void)showMsg:(NSString *)msg toView:(UIView *)view completionBlock:(void (^)(void))completion;

+ (void)showError:(NSString *)error toView:(UIView *)view;
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;

@end
