//
//  T_ValidCheck.m
//  PowerMail
//
//  Created by xiongyw on 13-11-20.
//  Copyright (c) 2013年 xiongyw. All rights reserved.
//

#import "X_ValidCheck.h"

@implementation X_ValidCheck

+(BOOL) isValid:(NSString *)checkString withRegex:(NSString *)regex
{
    NSPredicate *strTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [strTest evaluateWithObject:checkString];
}

+ (BOOL)isValidEmail:(NSString *)checkString
{
    //http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    bool stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    return [self isValid:checkString withRegex:emailRegex];
}

+ (BOOL)isValidPlateNo:(NSString *)checkString
{
    //NSString *regex = @"[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z0-9]{5}";
    NSString *regex = @"[京沪津渝黑吉辽蒙冀新甘青陕宁豫鲁晋皖鄂湘苏川贵云桂藏浙赣粤闽琼]{1}[a-zA-Z]{1}[a-zA-Z0-9]{5}";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidCellPhoneNo:(NSString *)checkString
{
    NSString *regex = @"1(3|4|5|7|8)[0-9]{9}";
    regex = @"1(2|3|4|5|7|8)[0-9]{9}";
    //支持当前已经开通的号码段
    //NSString *regex = @"1(3[\\d]{9}|5(0|1|2|3|5|6|8|9)[\\d]{8}|8[6-9]{1}[\\d]{8})";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidPostCode:(NSString *)checkString
{
    NSString *regex = @"[1-9][0-9]{5}";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidIDNO:(NSString *)checkString
{
    //NSString *regex = @"([\\d]{6}(19|20)[\\d]{2}((0[1-9])|1[012])(0[1-9]|[12][\\d]|(30|31))[\\d]{3}[xX\\d])|([\\d]{6}[\\d]{2}((0[1-9])|1[012])(0[1-9]|[12][\\d]|(30|31))[\\d]{3})";
    NSString *regex = @"([\\d]{6}(19|20)[\\d]{2}((0[1-9])|1[012])(0[1-9]|[12][\\d]|(30|31))[\\d]{3}[X\\d])|([\\d]{6}[\\d]{2}((0[1-9])|1[012])(0[1-9]|[12][\\d]|(30|31))[\\d]{3})";
    if (![self isValid:checkString withRegex:regex]) {
        return NO;
    }
    //return [self isValid:checkString withRegex:regex];
    
    /**
     * 身份证号码验证 1、号码的结构 公民身份号码是特征组合码，由十七位数字本体码和一位校验码组成。排列顺序从左至右依次为：六位数字地址码，
     * 八位数字出生日期码，三位数字顺序码和一位数字校验码。 2、地址码(前六位数）
     * 表示编码对象常住户口所在县(市、旗、区)的行政区划代码，按GB/T2260的规定执行。 3、出生日期码（第七位至十四位）
     * 表示编码对象出生的年、月、日，按GB/T7408的规定执行，年、月、日代码之间不用分隔符。 4、顺序码（第十五位至十七位）
     * 表示在同一地址码所标识的区域范围内，对同年、同月、同日出生的人编定的顺序号， 顺序码的奇数分配给男性，偶数分配给女性。 5、校验码（第十八位数）
     * （1）十七位数字本体码加权求和公式 S = Sum(Ai * Wi), i = 0, ... , 16 ，先对前17位数字的权求和
     * Ai:表示第i位置上的身份证号码数字值 Wi:表示第i位置上的加权因子 Wi: 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2
     * （2）计算模 Y = mod(S, 11) （3）通过模得到对应的校验码 Y: 0 1 2 3 4 5 6 7 8 9 10 校验码: 1 0 X 9 8 7 6 5 4 3 2
     */
    //18或15位身份证
    NSString *AreaCode[] = {
        @"11",
        @"12",
        @"13",
        @"14",
        @"15",
        @"21",
        @"22",
        @"23",
        @"31",
        @"32",
        @"33",
        @"34",
        @"35",
        @"36",
        @"37",
        @"41",
        @"42",
        @"43",
        @"44",
        @"45",
        @"46",
        @"50",
        @"51",
        @"52",
        @"53",
        @"54",
        @"61",
        @"62",
        @"63",
        @"64",
        @"65",
        @"71",
        @"81",
        @"82",
        @"91"};
    NSString *ValCodeArr[] = { @"1", @"0", @"X", @"9", @"8", @"7", @"6", @"5", @"4",
        @"3", @"2" };
    NSString *Wi[] = { @"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7",
        @"9", @"10", @"5", @"8", @"4", @"2" };
    NSString *Ai = @"";
    // ================ 号码的长度 15位或18位 ================
    if ([checkString length] != 15 && [checkString length] != 18) {
        return NO;
    }
    // ================ 数字 除最后以为都为数字 ================
    if ([checkString length] == 18) {
        Ai = [checkString substringToIndex:17];
    } else if ([checkString length] == 15) {
        Ai = [checkString substringToIndex:6];
        Ai = [Ai stringByAppendingFormat:@"19%@",[checkString substringFromIndex:6]];
    }
    if (![self isValidNumber:Ai]) {
        return NO;
    }
    
    // ================ 出生年月是否有效 ================
    NSString *strYear = [Ai substringWithRange:NSMakeRange(6,4)];// 年份
    NSString *strMonth = [Ai substringWithRange:NSMakeRange(10,2)];// 月份
    NSString *strDay = [Ai substringWithRange:NSMakeRange(12,2)];// 月份
    if (![self isDate:[strYear stringByAppendingFormat:@"-%@-%@",strMonth,strDay]]) {
        return NO;
    }
    
    //当前年月校验？
    if ([strMonth intValue] > 12 || [strMonth intValue] == 0) {
        return NO;
    }
    if ([strDay intValue] > 31 || [strDay intValue] == 0) {
        return NO;
    }
    
    // ================ 地区码时候有效 ================
    NSString *areaCode = [Ai substringWithRange:NSMakeRange(0,2)];
    BOOL flag = NO;
    for (int i = 0 ; i < 35; i++) {
        if ([AreaCode[i] isEqual:areaCode]) {
            flag = YES;
            break;
        }
    }
    if (!flag) {
        return NO;
    }
    
    // ================ 判断最后一位的值 ================
    int TotalmulAiWi = 0;
    for (int i = 0; i < 17; i++) {
        TotalmulAiWi = TotalmulAiWi + [[Ai substringWithRange:NSMakeRange(i,1)] intValue] * [Wi[i] intValue];
    }
    int modValue = TotalmulAiWi % 11;
    NSString  *strVerifyCode = ValCodeArr[modValue];
    Ai = [Ai stringByAppendingString:strVerifyCode];
    
    if ([checkString length] == 18) {
        if (![Ai isEqual:checkString]) {
            return NO;
        }
    }
    
    return YES;
}

+ (BOOL)isValidName:(NSString *)checkString
{
    //汉字2－30个或英文3－60个
    //NSString *regex = @"[\u4e00-\u9fa5]{2,30}|[A-Za-z][A-Za-z\\s]{2,29}|11111"; //后面的11111是为测试留的，是个缺陷
    //增加新疆人
    NSString *regex = @"[\u4e00-\u9fa5][\u4e00-\u9fa5|.|。]{1,29}|[A-Za-z][A-Za-z\\s]{2,59}|11111"; //后面的11111是为测试留的，是个缺陷
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidIDName:(NSString *)checkString
{
    NSString *regex = @"[\u4E00-\u9FA5]{2,5}(?:·[\u4E00-\u9FA5]{2,5})*";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidYear:(NSString *)checkString
{
    //1900-2099
    NSString *regex = @"(19|20)[\\d]{2}";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidNumber:(NSString *)checkString
{
    NSString *regex = @"^[0-9]+(.[0-9])?$";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidVIN:(NSString *)checkString
{
    //NSString *regex = @"[a-zA-Z0-9]{17}";
    NSString *regex = @"[a-hj-np-zA-HJ-NP-Z0-9]{17}";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isDate:(NSString *)checkString
{
    NSString *regex = @"^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$";
    return [self isValid:checkString withRegex:regex];
}

+ (BOOL)isValidLength:(NSString *)checkString min:(int)min max:(int) max {
    if ([checkString length] < min || [checkString length] > max) {
        return NO;
    }
    return YES;
}

@end
