//
//  X_Toast.h
//  旅行真人译
//
//  Created by xiongyw on 14-10-31.
//  Copyright (c) 2014年 CC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ToastDismissCompletion)(void);

@interface X_Toast : NSObject

+ (void)showErrorMsg:(NSString *)msg toView:(UIView *)view;

/**
 *  Progress
 *
 *  @param status <#status description#>
 */
+ (void)showWithStatus:(NSString*)status;

+ (void)showInfoWithStatus:(NSString*)status;

/**
 *  Success
 *
 *  @param status <#status description#>
 */
+ (void)showSuccessWithStatus:(NSString*)status;

+ (void)showSuccessWithStatus:(NSString*)status delay:(NSTimeInterval)delay;

+ (void)showSuccessWithStatus:(NSString*)status delay:(NSTimeInterval)delay completion:(ToastDismissCompletion)completion;


/**
 *  Error
 *
 *  @param status <#status description#>
 */
+ (void)showErrorWithStatus:(NSString*)status;

+ (void)showErrorWithStatus:(NSString*)status delay:(NSTimeInterval)delay;

+ (void)showErrorWithStatus:(NSString*)status delay:(NSTimeInterval)delay completion:(ToastDismissCompletion)completion;

@end
