//
//  ISSNavUtil.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSNavUtil.h"

@implementation ISSNavUtil

+ (NSString *)getImageName:(NSInteger)imageType {
    if (imageType == 1) return @"nav_back";
    return @"";
}

+ (UIImage *)getImage:(NSInteger)imageType {
    return [UIImage imageNamed:[self getImageName:imageType]];
}

+ (UIImage *)getImageHL:(NSInteger)imageType {
    return [UIImage imageNamed:FormatString(@"%@HL",[self getImageName:imageType])];
}

/**                                      */
+ (X_Button *)getImageBtn:(NSInteger)btnType {
    return [self getImageBtn:nil btnType:btnType clickAction:nil];
}

+ (X_Button *)getImageBtn:(id)delegate
                  btnType:(NSInteger)btnType
              clickAction:(SEL)clickAction
{
    UIImage *image = [self getImage:btnType];
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0.f, 0, image.size.width, image.size.height);
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:[self getImageHL:btnType] forState:UIControlStateHighlighted];
    if (clickAction) {
        [btn addTarget:delegate action:clickAction forControlEvents:UIControlEventTouchUpInside];
    }
    return btn;
}

+ (X_Button *)getTitleBtn:(NSString *)title {
    return [self getTitleBtn:nil btnTitle:title clickAction:nil];
}

+ (X_Button *)getTitleBtn:(id)delegate
                 btnTitle:(NSString *)btnTitle
              clickAction:(SEL)clickAction {
    X_Button *btn = [X_Button buttonWithType:UIButtonTypeCustom];
    
    [btn setTitleColor:[UIColor themeColor] forState:UIControlStateNormal];
    //[btn setTitleColor:[UIColor themeRedColorHL] forState:UIControlStateHighlighted];
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    [btn sizeToFit];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    if (clickAction) {
        [btn addTarget:delegate action:clickAction forControlEvents:UIControlEventTouchUpInside];
    }
    return btn;
}

+ (UIBarButtonItem *)getBarItem:(id)delegate
                        btnType:(NSInteger)btnType
                    clickAction:(SEL)clickAction
{
    UIButton *btn = [self getImageBtn:btnType];
    [btn addTarget:delegate action:clickAction forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    return bar;
}

+ (UIBarButtonItem *)getBarItem:(NSInteger)btnType
                     clickBlock:(X_ButtonBlock)block
{
    X_Button *btn = (X_Button *)[self getImageBtn:btnType];
    [btn setClickBlock:block];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    return bar;
}

+ (UIBarButtonItem *)getBarItemTitle:(NSString*)btnTitle
                          clickBlock:(X_ButtonBlock)block
{
    X_Button *btn = [X_Button new];
    if (btnTitle.length < 4) {
        btn.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    else
    {
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGBA(0xffffff, 0.5) forState:UIControlStateHighlighted];
    
    [btn sizeToFit];
    [btn setClickBlock:block];
    UIBarButtonItem *bar =[[UIBarButtonItem alloc] initWithCustomView:btn];
    
    return bar;
}

@end

