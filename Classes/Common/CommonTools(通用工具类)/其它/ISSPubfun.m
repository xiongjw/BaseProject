//
//  ISSPubfun.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSPubfun.h"

@implementation ISSPubfun

+ (NSString *)getApplicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    basePath = [basePath stringByAppendingPathComponent:@"Private"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    BOOL success = [fileManager fileExistsAtPath:basePath isDirectory:&isDir];
    if (!success || !isDir) {
        NSError *err;
        success = [fileManager createDirectoryAtPath:basePath
                         withIntermediateDirectories:YES
                                          attributes:nil
                                               error:&err];
        if (!success) {
            NSLog(@"Unresolved error %@, %@", err, [err userInfo]);
            exit(-1);
        }
    }
    //NSLog(@"====%@",basePath);
    return basePath;
}


+ (NSString *)getFolder:(NSString *)folder
{
    NSString *path = [[self getApplicationDocumentsDirectory] stringByAppendingPathComponent:folder];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ( [fm fileExistsAtPath:path] == false ) {
        [fm createDirectoryAtPath:path
      withIntermediateDirectories:YES
                       attributes:nil
                            error:NULL];
    }
    return path;
}

+ (void)modifyNavigationController:(UINavigationController *)navController
{
    [self modifyNavigationBar:navController.navigationBar];
}

+ (void)modifyNavigationBar:(UINavigationBar *)navigationBar
{
    [navigationBar setBackgroundColor:[UIColor clearColor]];
    navigationBar.tintColor = [UIColor whiteColor];
    
    navigationBar.shadowImage = [[UIImage alloc] init];
    navigationBar.translucent = NO;
    
    navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                         [UIColor whiteColor],NSForegroundColorAttributeName,
                                         [UIFont systemFontOfSize:18],NSFontAttributeName, nil];
    
    //UIImage *image = [UIImage imageWithColor:AXRGBA(0xffffff, 0.9)];
    UIImage *image = [UIImage imageWithColor:[UIColor themeColor]];
    [navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
}

+ (UIImage *)colorizeImage:(NSString *)baseImage
{
    UIImage *image = [UIImage imageNamed:baseImage];
    CGFloat scale = [UIScreen mainScreen].scale;
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/scale/2 - 1, image.size.width/scale/2 - 1, image.size.height/scale/2 - 1, image.size.width/scale/2 - 1)
                                 resizingMode:UIImageResizingModeStretch];
}

@end
