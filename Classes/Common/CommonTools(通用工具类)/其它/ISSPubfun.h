//
//  ISSPubfun.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/8.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSPubfun : NSObject

//+ (NSString *)getServerBaseUrl;

//+ (void) appSystemInitialization;

+ (NSString *)getApplicationDocumentsDirectory;

+ (NSString *)getFolder:(NSString *)folder;

+ (void)modifyNavigationController:(UINavigationController *)navController;

+ (void)modifyNavigationBar:(UINavigationBar *)navigationBar;

+ (UIImage *)colorizeImage:(NSString *)baseImage;

@end
