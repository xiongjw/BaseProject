//
//  UINavigationItem.m
//  旅行真人译
//
//  Created by xiongyw on 14-10-31.
//  Copyright (c) 2014年 CC. All rights reserved.
//

@implementation UINavigationItem (margin)

#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
- (void)setLeftBarButtonItem:(UIBarButtonItem *)_leftBarButtonItem
{
    //判断是不是系统默认
    if ( _leftBarButtonItem.customView == nil) {
        [self setLeftBarButtonItem:_leftBarButtonItem animated:NO];
    }
    else {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                               target:nil
                                                                                               action:nil];
            //negativeSeperator.width = -10;
            negativeSeperator.width = -5;
            
            if (_leftBarButtonItem)
                [self setLeftBarButtonItems:@[negativeSeperator, _leftBarButtonItem]];
            else
                [self setLeftBarButtonItems:@[negativeSeperator]];
        }
        else
            [self setLeftBarButtonItem:_leftBarButtonItem animated:NO];
    }
}

- (void)setRightBarButtonItem:(UIBarButtonItem *)_rightBarButtonItem
{
    //判断是不是系统默认
    if ( _rightBarButtonItem.customView == nil) {
        [self setRightBarButtonItem:_rightBarButtonItem animated:NO];
    }
    else {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            UIBarButtonItem *negativeSeperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                               target:nil
                                                                                               action:nil];
            //negativeSeperator.width = -12;
            negativeSeperator.width = -6;
            
            if (_rightBarButtonItem)
                [self setRightBarButtonItems:@[negativeSeperator, _rightBarButtonItem]];
            else
                [self setRightBarButtonItems:@[negativeSeperator]];
        }
        else
            [self setRightBarButtonItem:_rightBarButtonItem animated:NO];
    }
}

#endif
@end
