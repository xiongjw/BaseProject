//
//  UIImage+Create.h
//  AXGY
//
//  Created by xiongjw on 16/5/24.
//  Copyright © 2016年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Create)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
