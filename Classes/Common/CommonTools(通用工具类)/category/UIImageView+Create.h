//
//  UIImageView+Create.h
//  WH_O2O_U
//
//  Created by xiongjw on 15/8/29.
//  Copyright (c) 2015年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Create)

+ (UIImageView *)imageView:(CGRect)frame;

+ (UIImageView *)imageView:(CGRect)frame scale:(BOOL)scale;

+ (UIImageView *)imageView:(CGRect)frame image:(UIImage *)image;

+ (UIImageView *)imageView:(CGRect)frame image:(UIImage *)image scale:(BOOL)scale;
@end
