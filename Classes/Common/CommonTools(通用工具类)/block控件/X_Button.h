//
//  AppDelegate.h
//  AXGY
//
//  Created by xiongjw on 16/5/24.
//  Copyright © 2016年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^X_ButtonBlock)(void);
typedef void (^X_ButtonDragBlock)(void);

@interface X_Button : UIButton {
    BOOL _isDragging;
}

@property (nonatomic, strong) NSDictionary* userParam;

/**
 *  设置按钮点击事件
 *
 *  @param block 回调事件
 */
- (void)setClickBlock:(X_ButtonBlock)block;

/**
 *  设置按钮拖动事件
 *
 *  @param block 拖动事件
 */
- (void)setDragBlock:(X_ButtonDragBlock)block;

@end
