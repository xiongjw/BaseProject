//
//  ISSBaseVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSBaseVC.h"

@interface ISSBaseVC ()
{
    BOOL _showBackBtn;
}

@end

@implementation ISSBaseVC

#pragma mark - user method
- (void)setTitle:(NSString *)title showBackBtn:(BOOL)showBackBtn
{
    _showBackBtn = showBackBtn;
    self.navigationItem.title = title;
    
    if (showBackBtn) {
        //自定义返回按钮
        WeakSelf(self)
        self.navigationItem.leftBarButtonItem = [ISSNavUtil getBarItem:1 clickBlock:^{
            [weakself.view endEditing:YES];
            
            if (weakself.presentingViewController && [weakself isKindOfClass:[weakself.navigationController.viewControllers[0] class]]) {
                if (_dismissBlock) {
                    _dismissBlock();
                }
                [weakself dismissViewControllerAnimated:YES completion:NULL];
            }
            else {
                [weakself.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}

- (void)setTitle:(NSString *)title showBackBtn:(BOOL)showBackBtn dismissBlock:(void(^)(void))dismissBlock
{
    _dismissBlock = dismissBlock;
    [self setTitle:title showBackBtn:showBackBtn];
}

- (void)setTitle:(NSString *)title showBackBtn:(BOOL)showBackBtn didPopBlock:(void(^)(void))didPopBlock;
{
    _didPopBlock = didPopBlock;
    [self setTitle:title showBackBtn:showBackBtn];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = ISSBackgroundColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (![self.navigationController.viewControllers containsObject:self]) {
        if (_didPopBlock) {
            _didPopBlock();
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
