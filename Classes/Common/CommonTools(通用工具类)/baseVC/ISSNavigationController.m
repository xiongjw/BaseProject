//
//  ISSNavigationController.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSNavigationController.h"

@interface ISSNavigationController () <UIGestureRecognizerDelegate>

@end

@implementation ISSNavigationController

#pragma mark -  user method

- (void)customInitialization
{
    self.interactivePopGestureRecognizer.delegate = self;
    
    [ISSPubfun modifyNavigationController:self];
}

#pragma mark -  system method
- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self customInitialization];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customInitialization];
    }
    return self;
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.viewControllers[0] == self.topViewController) {
        return NO;
    }
    else if ([self.topViewController isKindOfClass:[ISSBaseVC class]]) {
        return !((ISSBaseVC *)self.topViewController).disablePopGesture;
    }
    
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
