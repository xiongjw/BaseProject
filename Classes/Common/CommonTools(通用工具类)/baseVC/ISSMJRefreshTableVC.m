//
//  ISSMJRefreshTableVC.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSMJRefreshTableVC.h"

@interface ISSMJRefreshTableVC () <UITableViewDataSource,UITableViewDelegate>

@end

@implementation ISSMJRefreshTableVC

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mTableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
    if ([self.mTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.mTableView setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }
}
#pragma mark -  user method
- (void)addHeaderAction
{
    self.mTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
}

- (void)headerRereshing {
    [self performSelector:@selector(requestDataReresh:) withObject:@"down"];
}

- (void)addFooterAction
{
    self.mTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    self.mTableView.mj_footer.hidden = YES;
}

- (void)footerRereshing {
    [self performSelector:@selector(requestDataReresh:) withObject:@"up"];
}

- (void)addHeaderAndFooterAction
{
    [self addHeaderAction];
    [self addFooterAction];
}

- (void)requestDataReresh:(NSString *)direction {
    
}

- (void)headerBeginRefreshing {
    [self.mTableView.mj_header beginRefreshing];
}

- (void)endRefreshing
{
    if (self.mTableView.mj_header.isRefreshing) {
        [self.mTableView.mj_header endRefreshing];
    }
    if (self.mTableView.mj_footer.isRefreshing) {
        [self.mTableView.mj_footer endRefreshing];
    }
}

- (void)endRefreshing:(NSString *)direction list:(NSArray *)list
{
    [self endRefreshing];
    
    if ([@"down" isEqualToString:direction]) {
        
        if (self.mTableView.mj_footer.hidden) {
            self.mTableView.mj_footer.hidden = NO;
            [self.mTableView.mj_footer resetNoMoreData];
        }
        [self.resultDataList removeAllObjects];
    }
    
    if (list.count < [PageSize integerValue])
    {   self.mTableView.mj_footer.hidden = YES;
        [self.mTableView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)addResultDataToList:(NSArray *)list direction:(NSString *)direction
{
    if ([@"down" isEqualToString:direction]) {
        
        if (self.mTableView.mj_footer.hidden) {
            self.mTableView.mj_footer.hidden = NO;
            [self.mTableView.mj_footer resetNoMoreData];
        }
        [self.resultDataList removeAllObjects];
    }
    
    if (list.count < [PageSize integerValue])
    {   self.mTableView.mj_footer.hidden = YES;
        [self.mTableView.mj_footer endRefreshingWithNoMoreData];
    }
    if (list.count > 0)
    {
        [self.resultDataList addObjectsFromArray:list];
        [self.mTableView reloadData];
    }
}

- (UITableView *)mTableView
{
    if (_mTableView == nil) {
        _mTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64) style:UITableViewStylePlain];
        _mTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _mTableView.tag = 99;
        _mTableView.dataSource = self;
        _mTableView.delegate = self;
        //_mTableView.separatorColor = [UIColor borderAndLineColor];
        
        [_mTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        _mTableView.backgroundColor = [UIColor clearColor];
        _mTableView.backgroundView = nil;
    }
    return _mTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.mTableView];
    
    self.resultDataList = [NSMutableArray new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSeparatorInset:UIEdgeInsetsZero];
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
