//
//  ISSBaseWebVC.h
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISSBaseWebVC : ISSBaseVC

@property (nonatomic, assign)   BOOL local; //是否本地文件
@property (nonatomic, assign)   BOOL isString; //是否字符串
@property (nonatomic,   copy)   NSString *url; //url链接
@property (nonatomic,   copy)   NSString *htmlString; //strring字符串
@property (nonatomic, assign)   BOOL share;//是否需要分享

@property (nonatomic, strong)   NSDictionary *shareInfo;

@end
