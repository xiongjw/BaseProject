//
//  ISSBaseTableVC.h
//  BaseProject
//
//  Created by xiongjw on 2018/3/30.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISSBaseVC.h"

@interface ISSBaseTableVC : ISSBaseVC

@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, strong) NSMutableArray *mutableTableData;
@property (nonatomic, strong) UITableView *mTableView;

- (instancetype)initWithStyle:(UITableViewStyle)style;

@end
