//
//  ISSUserData.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSUserData : NSObject

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userPhone;
@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *jpushRegistrationID;

+ (ISSUserData *)sharedInstance;
- (void)saveUserDataWithResponseData:(NSDictionary *)data;
- (void)clearUserData;

@end
