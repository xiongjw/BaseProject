//
//  ISSUserData.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/4.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSUserData.h"

#define ISSUSerDataKey @"ISSUSerDataKey"

@implementation ISSUserData


+ (ISSUserData *)sharedInstance
{
    static ISSUserData *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[ISSUserData alloc] init];
        
        NSDictionary *data = [ISSDataStorage getObject:ISSUSerDataKey];
        if (data) {
            [instance setValue:CheckStringNull(data[@"userId"]) forKey:@"userId"];
            [instance setValue:CheckStringNull(data[@"userPhone"]) forKey:@"userPhone"];
            [instance setValue:CheckStringNull(data[@"nickname"]) forKey:@"nickname"];
        }
    });
    return instance;
}

- (void)saveUserDataWithResponseData:(NSDictionary *)data
{
    if (data) {
        //保存相关数据
        [self setValue:CheckStringNull(data[@"userId"]) forKey:@"userId"];
        [self setValue:CheckStringNull(data[@"userPhone"]) forKey:@"userPhone"];
        [self setValue:CheckStringNull(data[@"nickname"]) forKey:@"nickname"];
        
        [ISSDataStorage saveObject:data key:ISSUSerDataKey];
    }
}

- (void)clearUserData
{
    [self setValue:@"" forKey:@"userId"];
    [self setValue:@"" forKey:@"userPhone"];
    [self setValue:@"" forKey:@"nickname"];
    
    [ISSDataStorage removeObject:ISSUSerDataKey];
}

//setter
- (void)setUserId:(NSString *)userId
{
    _userId = userId;
    
    NSMutableDictionary *oldDic = [NSMutableDictionary dictionaryWithDictionary:[ISSDataStorage getObject:ISSUSerDataKey]];
    [oldDic setObject:userId forKey:@"userId"];
    [ISSDataStorage saveObject:[oldDic copy] key:ISSUSerDataKey];
}

- (void)setUserPhone:(NSString *)userPhone
{
    _userPhone = userPhone;
    
    NSMutableDictionary *oldDic = [NSMutableDictionary dictionaryWithDictionary:[ISSDataStorage getObject:ISSUSerDataKey]];
    [oldDic setObject:userPhone forKey:@"userPhone"];
    [ISSDataStorage saveObject:[oldDic copy] key:ISSUSerDataKey];
}

- (void)setnikename:(NSString *)nikename
{
    _nickname = nikename;
    
    NSMutableDictionary *oldDic = [NSMutableDictionary dictionaryWithDictionary:[ISSDataStorage getObject:ISSUSerDataKey]];
    [oldDic setObject:CheckStringNull(nikename) forKey:@"nikename"];
    [ISSDataStorage saveObject:[oldDic copy] key:ISSUSerDataKey];
}

@end
