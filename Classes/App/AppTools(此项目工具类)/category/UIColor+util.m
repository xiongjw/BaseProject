//
//  UIColor+util.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/2.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "UIColor+util.h"

@implementation UIColor (util)

+ (UIColor *)themeColor
{
    return UIColorFromRGB(0x3464dd);
}

+ (UIColor *)describeColor
{
    return [UIColor darkGrayColor];
}

+ (UIColor *)borderColor
{
    return [UIColor lightGrayColor];
}

@end
