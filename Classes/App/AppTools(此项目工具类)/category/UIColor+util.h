//
//  UIColor+util.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/2.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (util)

+ (UIColor *)themeColor;

+ (UIColor *)describeColor;

+ (UIColor *)borderColor;

@end
