//
//  ISSCreateUIHelper.h
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISSCreateUIHelper : NSObject

+ (UIButton *)createSubmitBtnWithTitle:(NSString *)btnTitle posY:(CGFloat)posY;

@end
