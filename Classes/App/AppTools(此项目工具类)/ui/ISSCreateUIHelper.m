//
//  ISSCreateUIHelper.m
//  BaseProject
//
//  Created by xiongjw on 2018/4/3.
//  Copyright © 2018年 xiongjw. All rights reserved.
//

#import "ISSCreateUIHelper.h"

@implementation ISSCreateUIHelper

+ (UIButton *)createSubmitBtnWithTitle:(NSString *)btnTitle posY:(CGFloat)posY
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.frame = CGRectMake(30, posY, Screen_Width - 60, 44);
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    return btn;
}

@end
